<?php



use App\Model\Cross;
use App\Model\Horse;
use App\Model\Poney;
use App\Model\Rider;
use App\Model\Stable;
use App\Model\Address;
use App\Model\Manager;
use App\Model\Dressage;
use App\Model\Sheitland;

require_once __DIR__ . "/../src/app.php";

// MAIN PROGRAM


//ADRESSES

echo "ADRESSES \n"; 

echo "The good Adress \n";
try {
    echo $adre1 = new Address(5, "Leroy Street", 14000, "Caen");
} catch (Exception $e) {
   echo $e->getMessage();
}

echo "\n";

echo "A wrong postal Code \n";
try {
    echo $adre2 = new Address(99, "Bridge Street", -14000, "Falaise");
} catch (Exception $e) {
   echo $e->getMessage();
}

echo "\n";

//STABLE
echo "STABLE \n";
echo "_______________________________________________\n";

echo "Beaufiful Stable \n";

echo $stab1 = new Stable("Beautiful Stable", 5, "Leroy Street", 14000, "Caen");

echo "\n";

echo "PONEY \n";
echo "_______________________________________________\n";
echo "My first little poney \n";

//PONEY

try {
    echo $pon1 = new Poney("bai", 5, "Light", "poneygame");
} catch (Exception $e) {
    echo $e->getMessage();
}

echo "\n";

echo "My second little poney \n";

try {
    echo $pon2 = new Poney("GREY", 100, "Star", "dressage");
} catch (Exception $e) {
    echo $e->getMessage();
}

echo "\n"; 

echo "My negative little poney \n";

try {
    echo $pon3 = new Poney("GREY", -100, "Negative", "dressage");
} catch (Exception $e) {
    echo $e->getMessage();
}

echo "\n";

//HORSES
echo "HORSES \n";
echo "_______________________________________________\n";

echo "My first HORSE Youhouuu ! \n";

try {
    echo $hor1 = new Horse("grey", 500, "BigStar", "cross");
} catch (Exception $e) {
    echo $e->getMessage();
}

echo "\n"; 


echo "My Horse doing PoneyGame (really ?) \n ";
try {
    echo $hor2 = new Horse("grey", 500, "BigStar", "poneygame");
} catch (Exception $e) {
    echo $e->getMessage();
}

echo "SHEITLAND \n";
echo "_______________________________________________\n";


echo "My First Sheitland doing PoneyGame \n";
try {
    echo $shei1 = new Sheitland("grey", 500, "SheitMan", "pOnEyGame");
} catch (Exception $e) {
    echo $e->getMessage();
}


echo "MANAGER \n";
echo "_______________________________________________\n";

echo "My first Manager \n";


echo $man1 = new Manager("Marin", 9, "Falaise Street", 14000, "Caen", $stab1);

echo "RIDER \n";
echo "_______________________________________________\n";

echo "My First Rider \n";

echo $rid1 = new Rider("Vincent", 99, "Main Street", 14140, "MachinTruc");


echo "EVENTS \n";


echo "_______________________________________________\n";
echo "my First Event \n";

try {
    echo $dres1 = new Dressage(5, 100, "Dressage for the fun", "dreSSage");
} catch (Exception $e) {
    echo $e->getMessage();
}

echo "My second false event \n";

try {
    echo $dres2 = new Dressage(5, 100, "Dressage for the fun", "cross");
} catch (Exception $e) {
    echo $e->getMessage();
}

echo "My first cross event \n";

try {
    echo $cros1 = new Cross(5, 100, "Rick Cross", "cross");
} catch (Exception $e) {
    echo $e->getMessage();
}
