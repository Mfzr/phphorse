<?php

namespace App\Model;

use Exception;

    class Address {

        protected int $streetNumber;
        protected string $street;
        protected int $postalCode;
        protected string $city;

        public function __construct(int $streetNumber, string $street, int $postalCode, string $city)
        {
            $this->setStreetNumber($streetNumber)->setStreet($street)->setPostalCode($postalCode)->setCity($city);
        }

        /**
         * Get the value of streetNumber
         */ 
        public function getStreetNumber() : int
        {
                return $this->streetNumber;
        }

        /**
         * Set the value of streetNumber
         *
         * @return  self
         */ 
        public function setStreetNumber($streetNumber) : self
        {
                $this->streetNumber = $streetNumber;

                return $this;
        }

        /**
         * Get the value of street
         */ 
        public function getStreet() : string
        {
                return $this->street;
        }

        /**
         * Set the value of street
         *
         * @return  self
         */ 
        public function setStreet($street) : self
        {
                $this->street = $street;

                return $this;
        }

        /**
         * Get the value of postalCode
         */ 
        public function getPostalCode() : int
        {
                return $this->postalCode;
        }

        /**
         * Set the value of postalCode
         *
         * @return  self
         */ 
        public function setPostalCode($postalCode) : self
        {
                if ($this->checkPostCode($postalCode)) {

                        $this->postalCode = $postalCode;

                        return $this;
                }
                throw new Exception("The postal Code must be under 100 000 and more than 0 \n");
        }

        public function checkPostCode($postCode) : bool
        {
                if ($postCode > 0 && $postCode < 100000) return true;
                return false;
        }

        /**
         * Get the value of city
         */ 
        public function getCity() : string
        {
                return $this->city;
        }

        /**
         * Set the value of city
         *
         * @return  self
         */ 
        public function setCity($city) : self
        {
                $this->city = $city;

                return $this;
        }

        public function __toString() : string
        {
            return "The address is " . $this->getStreetNumber() . " " . $this->getStreet() . " at " . 
            $this->getCity() . " " . $this->getPostalCode() . " \n";
        }
    }