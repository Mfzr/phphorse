<?php

namespace App\Model;

use Exception;
use App\Model\Animal;

    abstract class Equine extends Animal {

        public const ARRAY_COLOR = ['Alzan', 'Bai', 'Pie', 'Grey', 'White'];

        private string $id;
        private string $color;
        private string $equineName;
        private int $water;
        protected static $equineCounting = 0;


        public function __construct(string $color, int $water, string $equineName)
        {
            $this->setColor($color)->setWater($water)->setEquineName($equineName)->setId($equineCounting, $equineNamen, $color);
            $this->addingHorses();
        }


        /**
         * Get the value of id
         */ 
        public function getId() : string
        {
                return $this->id;
        }

        /**
         * Set the value of id
         *
         * @return  self
         */ 
        public function setId($equineCounting, $equineName, $color) : self
        {
                $this->id = $equineCounting . "-" . $equineName[0] . "-" . $color[0];

                return $this;
        }

        /**
         * Get the value of color
         */ 
        public function getColor() : string
        {
                return $this->color;
        }

      
 
        /**
         * It sets the color of the horse.
         * 
         * @param color The color of the horse.
         *              If the color isnt in the table, return a message.
         * @return self The object itself.
         */
        public function setColor($color) : self
        {
            if ($this->checkColor($color)) {

                $this->color = $color;
                
                return $this;
            }
            throw new Exception("We don't know this color ! Please try a correct color");
        }


        /**
         * Get the value of water
         */ 
        public function getWater() : int
        {
                return $this->water;
        }

        /**
         * Set the value of water
         *
         * @return  self
         */ 
        public function setWater($water) : self
        {
            if ($this->checkWater($water)){

                $this->water = $water;
                return $this;
            }
            throw new Exception("The water can't be negative or equat at 0 !");
               
        }

        /**
         * > This function checks if the water level is above 0
         * 
         * @param water The amount of water in the tank.
         * 
         * @return True or False
         */
        public function checkWater($water) : bool
        {
            if ($water > 0) return true;
            return false;
        }

        /**
         * It checks if a color is in the array of colors
         * 
         * @param color The color of the horse.
         * 
         * @return True or False
         */
        public function checkColor($color) : bool
        {
            if (in_array($color, self::ARRAY_COLOR)) return true;
            return false;
        }

        /**
         * Get the value of name
         */ 
        public function getEquineName() : string
        {
                return $this->equineName;
        }

        /**
         * Set the value of name
         *
         * @return  self
         */ 
        public function setEquineName($equineName) : self
        {
                $this->equineName = $equineName;

                return $this;
        }
    

        /**
         * Get the value of equineCounting
         */ 
        public function getEquineCounting()
        {
                return self::$equineCounting;
        }

        private function addingHorses()
        {
            self::$equineCounting += 1;
        }


        public function __toString() : string
        {
            return "His name is " . $this->getEquineName() . " his color is " . 
            $this->getColor() . " he need to drink " . $this->getWater() . "L of water, and his ID is : " . $this->getId() . ". ";
        }

   
    }