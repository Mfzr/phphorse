<?php

namespace App\Model;

use App\Model\Address;

use Exception;

abstract class Human
{

    public const NAME_REGEX = '/^[a-zA-z]/';

    private string $humanName;
    protected Address $adress;


    public function __construct(string $humanName, int $streetNumber, string $street, int $postalCode, string $city)
    {
        $this->setHumanName($humanName)->setAdress($streetNumber, $street, $postalCode, $city);
    }

    /**
     * Get the value of humanName
     */
    public function getHumanName()
    {
        return $this->humanName;
    }

    /**
     * Set the value of humanName
     *
     * @return  self
     */
    public function setHumanName($humanName)
    {
        if ($this->checkHumanName($humanName)) {

            $this->humanName = $humanName;

            return $this;
        }
        throw new Exception("This human can only have letter in his name \n");
    }

    public function checkHumanName($humanName)
    {
        if (preg_match(self::NAME_REGEX, $humanName)) return true;
        return false;
    }


    /**
     * Get the value of adress
     */ 
    public function getAdress() : string
    {
        return $this->adress;
    }

    /**
     * Set the value of adress
     *
     * @return  self
     */ 
    private function setAdress(int $streetNumber, string $street, int $postalCode, string $city) : self
    {
        $this->adress = new Address($streetNumber, $street, $postalCode, $city);

        return $this;
    }



    public function __toString() : string
    {
        return "His name is " . $this->getHumanName();
    }

  
}
