<?php

use App\Model\Human;
use App\Model\Stable;

    class Manager extends Human {

        protected Stable $stable;

        public function __construct(string $humanName, int $streetNumber, string $street, int $postalCode, string $city, string $stableName)
        {
            parent::__construct($humanName, $streetNumber, $street, $postalCode, $city);
            $this->setStable($stableName, $streetNumber, $street, $postalCode, $city);
        }

        /**
         * Get the value of stable
         */ 
        public function getStable() : string
        {
                return $this->stable;
        }

        /**
         * Set the value of stable
         *
         * @return  self
         */ 
        public function setStable($stableName, $streetNumber, $street, $postalCode, $city) : self
        {
                $this->stable = new Stable($stableName, $streetNumber, $street, $postalCode, $city);

                return $this;
        }
    }