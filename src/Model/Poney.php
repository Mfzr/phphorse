<?php

namespace App\Model;

use App\Model\Equine;

    class Poney extends Equine {
        
        private const PONEY = "Poney";
        protected const COUNT_PONEY = [];

        public function __construct(string $id, string $color, int $water, string $equineName)
        {
            parent::__construct($id, $color, $water, $equineName);
        }

        public function __toString()
        {
            $msg = parent::__toString();

            return $msg .= "and its a " . self::PONEY . " \n";

        }
    }