<?php

namespace App\Model;

use App\Model\Human;

    class Rider extends Human {
        
        public function __construct(string $humanName)
        {
            parent::__construct($humanName);
        }

        public function __toString(): string
        {
            $msg = parent::__toString();

            return $msg;
        }
    }