<?php
namespace App\Model;

use App\Model\Address;

    class Stable {

        private string $stableName;
        protected Address $address;


        public function __construct(string $stableName, int $streetNumber, string $street, int $postalCode, string $city)
        {
            $this->setStableName($stableName)->setAddress($streetNumber, $street, $postalCode, $city);
        }

        /**
         * Get the value of stableName
         */ 
        public function getStableName() : string
        {
                return $this->stableName;
        }

        /**
         * Set the value of stableName
         *
         * @return  self
         */ 
        public function setStableName($stableName) : self
        {
                $this->stableName = $stableName;

                return $this;
        }

        public function __toString() : string
        {
            return "The Stable name is " . $this->getStableName() . "\n";
        }

        /**
         * Get the value of adress
         */ 
        public function getAdress()
        {
                return $this->adress;
        }

        /**
         * Set the value of adress
         *
         * @return  self
         */ 
        public function setAddress(int $streetNumber, string $street, int $postalCode, string $city)
        {
                $this->address = new Address($streetNumber, $street, $postalCode, $city);

                return $this;
        }
    }